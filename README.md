# Survey

Simple app for prediction candidate to be a panelist 

## Getting Started


### Prerequisites

For setup python in version min. 3.5 is required and venv

### Installing

A step by step series of examples that tell you how to get a development env running

Create virtual env

```
python3 -m venv venv
```

Activate venv

```
source venv/bin/acitvate
```

Install requirements

```
pip install -r requirements.txt
```

## Running the tests

*** TESTS WILL CREATE INITIAL DATABASE ***

To run tests simply run:

```
python -m unittest -v tests.py
```

## Deployment

To start application run:

```
python app.py 0.0.0.0:8080
```

Where 0.0.0.0 means that will bind to all IP addresses.
8080 is port number which web app will listen on.


## Using app

After starting application run browser and go to url http://127.0.0.1:8080.
You can see questions to answer, submit button and score information.
Every reload of page will put new data into local database, which is used to calculate score.

## Authors

Michał Gizowski

## License

This project is licensed under the MIT License
