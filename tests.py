import unittest
from settings import DB_FILE
from dateset import DataSet
import os


class TestDataSet(unittest.TestCase):

    def test_1_cleanup(self):
        try:
            os.unlink(DB_FILE)
        except FileNotFoundError:
            # All ok in first run
            pass
        return True

    def test_2_load_data(self):
        dataset = DataSet()
        dataset.load_data_from_csv("test.csv")
        self.assertEqual(dataset.count_results(), 100)

    def test_3_calculate(self):
        dataset = DataSet()
        res_not_full = dataset.calculate_probability(answers=[
                                                        'unemployed',
                                                        'previous_survey'])
        res_full = dataset.calculate_probability(answers=['unemployed',
                                                          'previous_survey',
                                                          'non_resident',
                                                          'underaged',
                                                          'imprisoned'])
        self.assertEqual(res_not_full, 37)
        self.assertEqual(res_full, 100)

    def test_4_insert_data(self):
        dataset = DataSet()
        dataset.insert(questions={'non_resident': 'T',
                                  'previous_survey': 'F',
                                  'underaged': 'U',
                                  'unemployed': 'T',
                                  'imprisoned': 'U'})
        self.assertEqual(dataset.count_results(), 101)

    def test_5_calculate_2(self):
        dataset = DataSet()
        res_not_full = dataset.calculate_probability(answers=[
                                                        'unemployed',
                                                        'previous_survey'])
        res_full = dataset.calculate_probability(answers=['unemployed',
                                                          'previous_survey',
                                                          'non_resident',
                                                          'underaged',
                                                          'imprisoned'])
        self.assertEqual(res_not_full, 36.633663366336634)
        self.assertEqual(res_full, 100)


if __name__ == '__main__':
    unittest.main()
