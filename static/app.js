
var questions;
var answers = [];

function generateUUID() {
    var d = new Date().getTime();
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;
        if(d > 0){
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

var session_id = generateUUID();

function clicked() {
    var without_no = this.id.replace(/^(no_)/,"");
    $.each([without_no, 'no_' + this.id, this.id], function (idx, val) {
        $("#" + val).css('color', 'black');
        var idx = answers.indexOf(val);
        if (idx > -1) {
            answers.splice(idx, 1);
        }

    });
    answers.push(this.id);
    $("#" + this.id).css('color', 'green');
    send_answers();
}

function send_answers() {
    $.ajax('ajax/put', {
    data : JSON.stringify([session_id, answers]),
    contentType : 'application/json',
    type : 'POST',
    success: function(data) {
        $( "#probability").text(data);
    },
    });
}

$( document ).ready(function() {
    $.getJSON("ajax/get", {session: session_id}).done(function(data) {
            questions = data;
            $.each( data, function( key, value ) {
                $("#questions").append("<br />" + value + '&nbsp;&nbsp;&nbsp;<span id=' + key + '>Yes</span>');
                $("#questions").append('&nbsp;&nbsp;&nbsp;<span id=\'no_' + key + '\'>No</span>');
            });

            var spans = document.getElementsByTagName('span');
            for(i=0;i<spans.length;i++)
                spans[i].onclick=clicked;
            });
    send_answers();

});
