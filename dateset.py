import sqlite3
import csv
from settings import DB_FILE


VAL_DICT = {'F': 'false',
            'T': 'true',
            'U': 'null',
            'Y': 'true',  # Found 1 case in input file
            }

KEYS = {
        'non_resident': 'Are You not resident of Poland?',
        'underaged': 'Are You -18 years old?',
        'unemployed': 'Are You unemployed?',
        'imprisoned': 'Are You imprisoned?',
        'previous_survey': 'Have you already completed the survey?'
}


def val_trans(val):
    """
    Translates value mostly used in csv files
    :param val: value like F,T,U,Y to be trnaslated otherwise 'null'
    """
    return VAL_DICT[val] if val in VAL_DICT.keys() else 'null'


class DataSet(object):

    def __init__(self):
        """
        Initializes object and creates local database located in the same dir
        """

        self.connection = sqlite3.connect(DB_FILE)
        self.cursor = self.connection.cursor()
        sql = "CREATE TABLE IF NOT EXISTS results ("
        for key in list(KEYS.keys()):
            sql += key + " boolean, "
        sql = sql[:-2] + ");"
        self.cursor.execute(sql)
        self.connection.commit()

    def count_results(self):
        """
        Returns amount of entries stored in local db
        :return: returns count
        """
        self.cursor.execute("""SELECT count(*) FROM results""")
        return int(self.cursor.fetchone()[0])

    def load_data_from_csv(self, filename):
        """
        Loads data from given csv file
        :param filename: The name and path of file
        :return: returns nothing
        """
        with open(filename) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            headers = None
            values = None
            for line_no, line in enumerate(reader):
                if line_no == 0:
                    headers = [x.replace(' ', '_') for x in line]
                    continue
                values = [val_trans(x) for x in line]
                self.cursor.execute("""INSERT INTO results ({}) VALUES ({})"""
                                    .format(','.join(headers),
                                            ','.join(values)))
        self.connection.commit()

    def get_data(self):
        """
        Returns data from local db
        :return: returns all data
        """
        self.cursor.execute("SELECT * FROM results")
        return self.cursor.fetchall()

    def insert(self, questions={}):
        """
        Insert one set of answers
        :param questions: Dict with given answers (keys have to be the same
        as definied below in keys list)
        :return: returns True or False as a result of successfull insert
        """
        keys = list(KEYS.keys())
        if sorted(questions.keys()) != sorted(keys):
            return False
        values = [val_trans(x) for x in questions.values()]
        self.cursor.execute("""INSERT INTO results ({}) VALUES ({})"""
                            .format(','.join(questions.keys()),
                                    ','.join(values)))
        self.connection.commit()
        return True

    def calculate_probability(self, answers=[]):
        """
        Functions returns probability of next candidate that will be panelist
        :param answer: Dict with already answered questions
                        (give only false answers)
        :return: dict with probability summary
        """
        if len(answers) >= 5:
            return 100.0
        keys = list(KEYS.keys())
        for answer in answers:
            keys.remove(answer)
        left = list(filter(lambda x: x in answers, keys))
        cnt = self.count_results()
        sql = "SELECT count(*) FROM results WHERE "
        for idx, answer in enumerate(keys):
            if idx > 0:
                sql += " AND NOT " + answer
            else:
                sql += "NOT " + answer
        self.cursor.execute(sql)
        amount = self.cursor.fetchone()[0]
        return amount / cnt * 100
