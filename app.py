import web
import json
from web.contrib.template import render_mako
from dateset import KEYS, DataSet

urls = ("/ajax/get", "Get",
        "/ajax/put", "Put",
        "/.*", "Index",
        )

app = web.application(urls, globals())
render = render_mako(
        directories=['templates'],
        input_encoding='utf-8',
        output_encoding='utf-8',
        )

last_session = None
last_answers = []


class Index:

    def GET(self):
        return render.index()


class Get:

    def GET(self):
        inp = web.input()
        return json.dumps(KEYS)


class Put:

    def __init__(self):
        self.dataset = DataSet()

    def POST(self):
        global last_session
        global last_answers
        session_id, answers = json.loads(web.data())
        if session_id != last_session and last_session is not None:
            dict_answers = {}
            for key in KEYS.keys():
                if key in last_answers:
                    dict_answers[key] = 'T'
                elif 'no_' + key in last_answers:
                    dict_answers[key] = 'F'
                else:
                    dict_answers[key] = 'U'
            self.dataset.insert(questions=dict_answers)

        filtered_answers_yes = list(filter(lambda x: not x.startswith('no_'),
                                           answers))
        filtered_answers_no = [x[3:] for x in list(
                               filter(lambda x: x.startswith('no_'), answers))]
        last_answers = answers
        last_session = session_id
        if len(filtered_answers_yes) > 0:
            return "0%"
        return "{:.2f}".format(self.dataset.calculate_probability(
                                    answers=filtered_answers_no)) + '%'


if __name__ == "__main__":
    app.run()
